<?php
// Landing Page
$f3->route('GET /', 'Controller\IndexController->index');

// User Routes
$f3->route('GET /user', 'Controller\UserController->index');
$f3->route('GET /user/@uid', 'Controller\UserController->show');

$f3->route('GET /user/new', 'Controller\UserController->store');
$f3->route('POST /user/new', 'Controller\UserController->store');

$f3->route('GET /user/@uid/edit', 'Controller\UserController->edit');
$f3->route('POST /user/@uid/edit', 'Controller\UserController->edit');

$f3->route('GET /user/@uid/delete', 'Controller\UserController->delete');

// Courses Routes
$f3->route('GET /courses', 'Controller\CoursesController->index');
$f3->route('GET /courses/new', 'Controller\CoursesController->store');
$f3->route('POST /courses/new', 'Controller\CoursesController->store');

// Login Routes
$f3->route('GET /login', 'Controller\LoginController->index');
