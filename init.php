<?php
require_once __DIR__ . '/vendor/autoload.php';

$f3 = \Base::instance();

// Routes
require_once __DIR__ . '/routes.php';
$f3->run();
