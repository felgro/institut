<?php

namespace Controller;

use \Template;

class LoginController
{
    public function index($f3, $params)
    {
        $f3->set('pageTitle', 'Login');
        $f3->set('header', 'Login');

        $f3->set('content', '/views/content/login.html');

        echo Template::instance()->render('/views/index.html');
    }
}
