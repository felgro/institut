<?php

namespace Controller;
use \Template;

class CoursesController {

    public function index($f3, $params)
    {
        $cm = new \Models\CoursesModel();
        $tm = new \Models\TrainerModel();
        $rm = new \Models\RoomModel();

        $data = [];

        $courses = $cm->courses();
        foreach($courses as $course) {

            $room = $rm->getRoom($course['room_id']);
            $trainer = $tm->getTrainer($course['trainer_id']);

            array_push($data, [
                'title' => $course['title'],
                'desc' => $course['description'],
                'date' => $course['date'],
                'time' => $course['time_start'] . ' - ' . $course['time_end'],
                'room' => $room['description'] . ' (' . $room['number'] . ')',
                'trainer' => $trainer['name']
            ]);
        }

        $f3->set('courses', $data);


        $f3->set('pageTitle', 'Kurse');
        $f3->set('header', 'Kurs Übersicht');

        $f3->set('content', '/views/content/courses.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function store($f3, $params)
    {
        $cm = new \Models\CoursesModel();
        $tm = new \Models\TrainerModel();
        $rm = new \Models\RoomModel();

        if(!empty($_POST)) {

            $gump = new \GUMP();

            $gump->validation_rules(array(
                'title'         => 'required|max_len,100|min_len,2',
                'desc'          => 'required|max_len,65535|min_len,5',
                'trainer'       => 'required|integer',
                'room'          => 'required|integer',
                'date'          => 'required|date|max_len,12',
                'time_start'    => 'required|max_len,5',
                'time_end'      => 'required|max_len,5'
            ));

            $data = $gump->run($_POST);

            if($data === false) {
                $f3->set('values', $_POST);
                $f3->set('errors', $gump->get_errors_array());
            } else {

                if($cm->store($data)) {
                    $f3->set('alertSuccess', 'Kurs erfolgreich angelegt!');
                } else {
                    $f3->set('alertError', 'Fehler beim anlegen');
                }
            }
        }
        
        $f3->set('trainer', $tm->trainer());
        $f3->set('rooms', $rm->rooms());

        $f3->set('pageTitle', 'Kurs hinzufügen');
        $f3->set('header', 'Kurs hinzufügen');

        $f3->set('content', '/views/content/courses-store.html');

        echo Template::instance()->render('/views/index.html');
    }


}