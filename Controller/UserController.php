<?php

namespace Controller;

use Exception;
use \Template;

class UserController
{

    public function index($f3, $params)
    {

        $um = new \Models\UserModel();
        $f3->set('users', $um->users());

        $f3->set('pageTitle', 'Übersicht');
        $f3->set('header', 'User Übersicht');

        $f3->set('scripts', [
            'user.js'
        ]);

        $f3->set('content', '/views/content/user.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function show($f3, $params)
    {
        // User ID
        $id = (int) $params['uid'] ?? 0;

        if (!is_int($id)) {
            throw new Exception('User ID should be defined as Integer');
        }

        $um = new \Models\UserModel();

        $usr = $um->user($id);

        if (count($usr) === 1) {
            $f3->set('usr', $usr[0]);
        } else {
            throw new Exception('user not found');
        }


        $f3->set('pageTitle', 'Benutzer');
        $f3->set('header', 'Benutzer');

        $f3->set('content', '/views/content/user-show.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function store($f3, $params)
    {
        $post = $f3->get('POST');

        // Prüft ob POST Array existiert
        if (!empty($post)) {

            $errors = [];

            // Validierung mit GUMP
            $gump = new \GUMP('de');
            $post = $gump->sanitize($post);

            $gump->validation_rules(array(
                'email'       => 'required|valid_email',
                'password'    => 'required|max_len,100|min_len,6'
            ));

            $data = $gump->run($_POST);

            if ($data === false) {
                $errors = $gump->get_errors_array();

                $f3->set('errors', $errors);
                $f3->set('values', $post);
            } else {
                $um = new \Models\UserModel();

                if ($um->storeUser($data['email'], $data['password'])) {
                    $f3->set('alertSuccess', 'Neuer Benutzer angelegt');
                } else {
                    $f3->set('alertError', 'Neuer Benutzer konnte nicht angelegt werden.');
                }
            }
        }

        $f3->set('pageTitle', 'Neuer User');
        $f3->set('header', 'Registrieren');

        $f3->set('content', '/views/content/user-store.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function edit($f3, $params)
    {
        $post = $f3->get('POST');

        if (!empty($post)) {

            $errors = [];

            // Validierung mit GUMP
            $gump = new \GUMP('de');
            $post = $gump->sanitize($post);

            $gump->validation_rules(array(
                'email'       => 'required|valid_email',
                'password'    => 'required|max_len,100|min_len,6'
            ));

            $data = $gump->run($_POST);

            if ($data === false) {
                $errors = $gump->get_errors_array();

                $f3->set('errors', $errors);
                $f3->set('values', $post);
            } else {
                $um = new \Models\UserModel();

                if ($um->updateUser($data['id'], $data['email'], $data['password'])) {
                    $f3->set('alertSuccess', 'Benutzer wurde aktualisiert');
                } else {
                    $f3->set('alertError', 'Fehler');
                }
            }
        }
        // User ID
        $id = (int) $params['uid'] ?? 0;

        if (!is_int($id)) {
            throw new Exception('User ID should be defined as Integer');
        }

        $um = new \Models\UserModel();

        $usr = $um->user($id);

        $f3->set('user', $usr[0]);

        $f3->set('pageTitle', 'Benutzer Bearbeiten');
        $f3->set('header', 'Eintrag bearbeiten');

        $f3->set('content', '/views/content/user-edit.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function delete($f3, $params)
    {
        $id = (int) $params['uid'] ?? 0;

        if (filter_var($id, FILTER_VALIDATE_INT)) {

            $um = new \Models\UserModel();

            $usr = $um->user($id)[0];
            if ($um->deleteUser($id)) {
                echo 1;
            } else {
                echo 0;
            };
        } else {
            echo 0;
        }
    }
}
