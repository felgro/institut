<?php

namespace Controller;

use \Template;

class IndexController
{
    public function index($f3, $params)
    {
        $f3->set('pageTitle', 'Routing Engine');
        $f3->set('header', 'F3 Template Engine');
        $f3->set('username', 'Felix');

        $f3->set('content', '/views/content/home.html');

        echo Template::instance()->render('/views/index.html');
    }
}
