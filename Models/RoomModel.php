<?php

namespace Models;

class RoomModel extends Model {
    
    public function rooms()
    {
        return $this->db->exec(
            'SELECT * FROM rooms'
        );
    }

    public function getRoom($id)
    {
        return $this->db->exec(
            'SELECT * FROM rooms WHERE id=?',
            $id
        )[0];
    }
}