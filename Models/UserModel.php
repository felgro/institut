<?php

namespace Models;

class UserModel extends Model
{
    public function users()
    {
        return $this->db->exec('SELECT * FROM user');
    }

    public function user(int $id): array
    {
        return $this->db->exec(
            'SELECT * FROM user WHERE id=?',
            $id
        );
    }

    public function storeUser(string $mail, string $password): bool
    {
        return $this->db->exec(
            'INSERT INTO user (email, password) VALUES (?, ?)',
            array($mail, $password)
        );
    }

    public function updateUser($id, string $mail, string $password): bool
    {
        return $this->db->exec(
            'UPDATE `user` SET `email` = ?, `password` = ? WHERE `user`.`id` = ?',
            array($mail, $password, $id)
        );
    }

    public function deleteUser($id): bool
    {
        return $this->db->exec(
            'DELETE FROM `user` WHERE `user`.`id` = ?',
            $id
        );
    }
}
