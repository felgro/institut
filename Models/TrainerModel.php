<?php

namespace Models;

class TrainerModel extends Model {

    public function trainer()
    {
        return $this->db->exec(
            'SELECT * FROM trainer'
        );
    }

    public function getTrainer($id)
    {
        return $this->db->exec(
            'SELECT * FROM trainer WHERE id=?',
            $id
        )[0];
    }
}