<?php

namespace Models;

class CoursesModel extends Model
{
    public function courses()
    {
        return $this->db->exec(
            'SELECT * FROM `courses`'
        );
    }

    public function store(array $data) {
        return $this->db->exec(
            'INSERT INTO `courses` (`trainer_id`, `room_id`, `title`, `description`, `date`, `time_start`, `time_end`) VALUES (?, ?, ?, ?, ?, ?, ?)',
            array(
                $data['trainer'],
                $data['room'],
                $data['title'],
                $data['desc'],
                $data['date'],
                $data['time_start'],
                $data['time_end']
            )
            );
    }
}