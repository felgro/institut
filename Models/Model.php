<?php

namespace Models;

abstract class Model
{
    protected $db;

    // TODO: Zugangsdaten in die init.php verlagern
    public function __construct()
    {
        // DB Verbindung als Klassenattribut speichern
        $this->db = new \DB\SQL(
            'mysql:host=localhost;port=3308;dbname=institut',
            'root',
            ''
        );
    }
}
