(() => {
  let btnDelete = $(".btn-delete");

  btnDelete.click(function(e) {
    let deleteOK = confirm("Soll dieser Benutzer wirklick gelöscht werden ?");

    if (!deleteOK) {
      return;
    }

    let button = $(this);

    let url = button.data("url");

    $.get(url, function(data) {
      if (data === "1") {
        button.closest("tr").remove();
      } else {
        // AJAX ERROR HANDLING
        console.log("AJAX Error");
      }
    });
  });
})();
